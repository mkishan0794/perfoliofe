import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashsidenavComponent } from './dashsidenav.component';

describe('DashsidenavComponent', () => {
  let component: DashsidenavComponent;
  let fixture: ComponentFixture<DashsidenavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashsidenavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashsidenavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

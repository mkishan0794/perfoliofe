import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { IndexComponent } from './index/index.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { ProfileComponent } from './profile/profile.component';
import { CreateprofileComponent } from './createprofile/createprofile.component';
import { AddinterestsComponent } from './addinterests/addinterests.component';
import { AddsocialComponent } from './addsocial/addsocial.component';
import { DomoreComponent } from './domore/domore.component';
import { DashnavComponent } from './dashnav/dashnav.component';
import { DashsidenavComponent } from './dashsidenav/dashsidenav.component';
import { AboutmeComponent } from './aboutme/aboutme.component';
import { UsercreateComponent } from './usercreate/usercreate.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdcontainerComponent } from './adcontainer/adcontainer.component';

import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    IndexComponent,
    ResetpasswordComponent,
    ProfileComponent,
    CreateprofileComponent,
    AddinterestsComponent,
    AddsocialComponent,
    DomoreComponent,
    DashnavComponent,
    DashsidenavComponent,
    AboutmeComponent,
    UsercreateComponent,
    DashboardComponent,
    AdcontainerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

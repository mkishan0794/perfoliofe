import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


interface LoginResponse {
  user         : any,
  token        : string,
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public apiRoot   : string = "http://localhost:8000";

  public loggedInStatus = false;

  constructor(private http: HttpClient) { }

  login(username, password) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    let url = `${this.apiRoot}/auth/login/`;
    return this.http.post<LoginResponse>(url,{
      username,
      password
    },
    {
      headers: headers
    })
  }

  signup(username, email, password1, password2) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    let url = `${this.apiRoot}/auth/signup/`;
    return this.http.post<LoginResponse>(url,{
      username,
      email,
      password1,
      password2
    },
    {
      headers: headers
    })
  }

  resetpassword(email) {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    let url = `${this.apiRoot}/auth/password-reset/`;
    return this.http.post<LoginResponse>(url,{
      email
    },
    {
      headers: headers
    })
  }

  setLoggedIn(value: boolean){
    this.loggedInStatus = value
  }

  get isLoggedIn(){
    return this.loggedInStatus 
  } 

}

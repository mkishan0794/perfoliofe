import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private Auth: AuthService) { }

  ngOnInit() {
  }

  signupUser(event){
    event.preventDefault()
    const target = event.target
    const username  = target.querySelector('#username').value
    const email     = target.querySelector('#email').value
    const password1 = target.querySelector('#password1').value
    const password2 = target.querySelector('#password2').value


    this.Auth.signup(username, email, password1, password2)
    .subscribe(
      data=> {
        //Go to Dashboard
        // localStorage.setItem('perfolioAuth', data.token);
        // localStorage.setItem('perfolioUname', data.user.username)
        console.log(data)
        // console.log(data.user.username);
      },
      error=> {
        console.log(error.error)
      })
  }

}

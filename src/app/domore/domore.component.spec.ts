import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomoreComponent } from './domore.component';

describe('DomoreComponent', () => {
  let component: DomoreComponent;
  let fixture: ComponentFixture<DomoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomoreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

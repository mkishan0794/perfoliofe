import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddinterestsComponent } from './addinterests.component';

describe('AddinterestsComponent', () => {
  let component: AddinterestsComponent;
  let fixture: ComponentFixture<AddinterestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddinterestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddinterestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

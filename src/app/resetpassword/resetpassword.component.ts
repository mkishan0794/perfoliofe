import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  constructor(private Auth: AuthService) { }

  ngOnInit() {
  }

  resetPassword(event){
    event.preventDefault()
    const target    = event.target
    const email     = target.querySelector('#email').value
    

    this.Auth.resetpassword(email)
    .subscribe(
      data=> {
        //Go to Dashboard
        // localStorage.setItem('perfolioAuth', data.token);
        // localStorage.setItem('perfolioUname', data.user.username)
        console.log(data);
        // console.log(data.user.username);
      },
      error=> {
        console.log(error.error)
      })
    // console.log(username, password)
  }

}
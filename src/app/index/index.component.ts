import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  constructor(private Auth: AuthService, private router: Router) { }

  ngOnInit() {
  }

  loginUser(event){
    event.preventDefault()
    const target = event.target
    const username = target.querySelector('#login-username').value
    const password = target.querySelector('#login-password').value


    this.Auth.login(username, password)
    .subscribe(
      data=> {
        //Go to Dashboard
        localStorage.setItem('perfolioAuth', data.token);
        localStorage.setItem('perfolioUname', data.user.username)
        this.router.navigate(['username/dashboard'])
        this.Auth.setLoggedIn(true)
        // console.log(data.token);
        // console.log(data.user.username);
      },
      error=> {
        console.log(error)
        console.log(error.error)
      })
  }

  signupUser(event){
    event.preventDefault()
    const target = event.target
    const username  = target.querySelector('#username').value
    const email     = target.querySelector('#email').value
    const password1 = target.querySelector('#password1').value
    const password2 = target.querySelector('#password2').value


    this.Auth.signup(username, email, password1, password2)
    .subscribe(
      data=> {
        //Go to Dashboard
        // localStorage.setItem('perfolioAuth', data.token);
        // localStorage.setItem('perfolioUname', data.user.username)
        console.log(data)
        // console.log(data.user.username);
      },
      error=> {
        console.log(error.error)
      })
  }

}

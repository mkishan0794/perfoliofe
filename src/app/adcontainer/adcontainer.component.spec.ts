import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdcontainerComponent } from './adcontainer.component';

describe('AdcontainerComponent', () => {
  let component: AdcontainerComponent;
  let fixture: ComponentFixture<AdcontainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdcontainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdcontainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
